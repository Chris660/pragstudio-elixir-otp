defmodule Servy.Bear do
  alias Servy.Bear

  defstruct id: nil, name: "", type: "", hibernating: false

  def is_grizzly(%Bear{} = bear) do
    bear.type == "Grizzly"
  end

  def name_lower?(%Bear{} = a, %Bear{} = b) do
    a.name < b.name
  end
end
