defmodule Servy.PledgeController do
  alias Servy.PledgeServer
  alias Servy.View

  def new(conv) do
    # Render the new pledge form, results posted to `create/2`
    %{conv | status: 200, resp_body: View.render("new_pledge.eex")}
  end

  def create(conv, %{"name" => name, "amount" => amount}) do
    # Sends the request to the remote service and caches it.
    PledgeServer.create_pledge(name, String.to_integer(amount))

    %{conv | status: 201,
      resp_body: "Thankyou #{name} - pledge #{amount}!"}
  end

  def index(conv) do
    # Return the recent pledges from the cache.
    {:ok, pledges} = PledgeServer.recent_pledges()

    %{conv | status: 200,
      resp_body: Servy.View.render("recent_pledges.eex", pledges: pledges)}
  end
end
