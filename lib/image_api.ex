defmodule ImageApi do
  require HTTPoison
  require Poison

  def query(img_id) do
    url = "https://api.myjson.com/bins/" <> img_id
    with {:ok, json}    <- fetch(url),
         {:ok, img_url} <- decode(json) do
      {:ok, img_url}
    end
  end

  defp fetch(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: status, body: body}} ->
        handle_http_response(status, body)
      {:error, %HTTPoison.Error{} = error} ->
        handle_http_error(error)
    end
  end

  defp handle_http_response(200, body) do
    {:ok, body}
  end

  defp handle_http_response(status, _body) do
    {:error, "GET failed: #{status}"}
  end

  defp handle_http_error(%HTTPoison.Error{} = error) do
    {:error, "Connection error: #{inspect error.reason}"}
  end

  defp decode(json_data) do
    # TODO: try..except
    json_map = Poison.Parser.parse!(json_data, %{})
    case get_in(json_map, ["image", "image_url"]) do
      nil -> {:error, "malformed data: #{inspect json_map}"}
      url -> {:ok, url}
    end
  end
end
