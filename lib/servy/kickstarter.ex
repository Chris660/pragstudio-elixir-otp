defmodule Servy.Kickstarter do
  use GenServer
  alias Servy.HttpServer

  def start_link(_) do
    IO.puts("Starting the KickStarter...")
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def get_server() do
    GenServer.call(__MODULE__, :get_server)
  end

  # ------------------

  def init(:ok) do
    Process.flag(:trap_exit, true)
    http_pid = spawn_server()
    {:ok, http_pid}
  end

  def handle_info({:EXIT, pid, why}, _state) do
    IO.puts("HTTP server with pid #{inspect pid} "
      <> "terminated: #{inspect why}") 
    new_http_pid = spawn_server()
    {:noreply, new_http_pid}
  end

  def handle_call(:get_server, _from, state) do
    {:reply, state, state}
  end

  defp spawn_server() do
    port = Application.get_env(:servy, :port)
    http_pid = spawn_link(HttpServer, :start, [port])
    Process.register(http_pid, :http_server)
    http_pid
  end
end
