defmodule Servy.Parser do
  def parse(request) do
    [header, body] = String.split(request, "\r\n\r\n")
    [request | headers] = String.split(header, "\r\n")
    [method, path, _protocol] = String.split(request, " ")
    header_map = parse_headers(headers)
    params_map = parse_params(header_map["Content-Type"], body)

    %Servy.Conv{
      method: method,
      path: path,
      headers: header_map,
      params: params_map
    }
  end

  @doc """
  Parses the given URL encoded parameters in to a map of key/value
  string pairs.

  ## Examples
    iex> params = "name=Baloo&type=Brown"
    iex> Servy.Parser.parse_params("application/x-www-form-urlencoded", params)
    %{"name" => "Baloo", "type" => "Brown"}
    iex> Servy.Parser.parse_params("multipart/form-data", params)
    %{}
  """
  def parse_params("application/x-www-form-urlencoded", params) do
    params |> String.trim() |> URI.decode_query()
  end

  def parse_params("application/json", params) do
    params |> Poison.decode!
  end

  def parse_params(_, _), do: %{}

  def parse_headers(header_list) do
    Enum.reduce(header_list, %{},
      fn line, acc -> put_header(acc, line) end)
  end

  defp put_header(map, line) do
    [key, val] = String.split(line, ~r{: *}, trim: true, parts: 2)
    Map.put(map, key, val)
  end
end
