defmodule Servy.Api.BearController do
  alias Servy.Response

  def index(conv) do
    json = Servy.Wildthings.list_bears() |> Poison.encode!()
    conv =
      Response.Utils.put_resp_header(conv, "Content-Type", "application/json")

    %{conv | status: 200, resp_body: json}
  end

  def create(conv, %{"name" => name, "type" => type}) do
    # TODO: create a bear
    %{conv |
      status: 201,
      resp_body: "Created a #{type} bear named #{name}!"}
  end
end
