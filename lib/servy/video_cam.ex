defmodule Servy.VideoCam do
  def snapshot(camera) do
    :timer.sleep(1000)
    "snapshot-#{camera}-#{:rand.uniform(1000)}.jpg"
  end
end
