defmodule Servy.HttpClient do

  @doc """
  synchronous HTTP `request` to `host`:`port`.
  """
  def request(host, port, request) do
    {:ok, sock} = :gen_tcp.connect(host, port, [:binary, active: false], 1000)
    :ok = :gen_tcp.send(sock, request)
    {:ok, response} = :gen_tcp.recv(sock, 0, 1000)
    :ok = :gen_tcp.close(sock)
    response
  end
end
