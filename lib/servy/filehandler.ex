defmodule Servy.FileHandler do
  @server_root Path.expand("../..", __DIR__)
  @static_root Path.join(@server_root, "pages")

  require Earmark
  alias Servy.Conv

  def serve_static(filename, %Conv{} = conv) do
    @static_root
    |> Path.join(filename)
    |> File.read()
    |> handle_file_read(filename, conv)
  end

  defp handle_file_read({:ok, content}, filename, conv) do
    body = render(Path.extname(filename), content)
    %{conv | :status => 200, :resp_body => body}
  end

  defp handle_file_read({:error, reason}, _filename, conv) do
    case reason do
      :enoent -> %{conv | :status => 404}
      :eacces -> %{conv | :status => 403}
      _ -> %{conv | :status => 500}
    end
  end

  defp render(".md", content), do: Earmark.as_html!(content)
  defp render(".markdown", content), do: Earmark.as_html!(content)
  defp render(_, content), do: content
end
