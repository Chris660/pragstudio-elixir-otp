defmodule Servy.PledgeServer do

  @server_name :pledge_server

  # By "using" GenServer we inject default callbacks for the behaviour.
  use GenServer

  defmodule State do
    defstruct cachesize: 3, recent: [], total: 0
  end

  #
  # Client interface functions
  #

  def start_link(_arg) do
    IO.puts "starting the pledge server..."
    GenServer.start_link(__MODULE__, %State{}, name: @server_name)
  end

  def stop() do
    GenServer.stop(@server_name)
  end

  def create_pledge(name, amount) do
    call {:create_pledge, name, amount}
  end

  def recent_pledges do
    call :recent_pledges
  end

  def total_pledged do
    call :total_pledged
  end

  def clear, do: cast(:clear)

  def set_cache_size(size) when is_integer(size) and size > 0 do
    cast {:set_cache_size, size}
  end

  defp call(request) do
    GenServer.call(@server_name, request, 1000)
  end

  defp cast(request) do
    GenServer.cast(@server_name, request)
  end

  # Server callbacks
  def init(state) do
    {:ok, %State{state | recent: fetch_recent_pledges()}}
  end

  def handle_call({:create_pledge, name, amount}, _from, state) do
    case send_pledge_to_service(name, amount) do
      {:ok, id} ->
        cache_tail = Enum.take(state.recent, state.cachesize - 1)
        {:reply, {:ok, id},
          %State{state |
          :recent => [{id, name, amount} | cache_tail],
          :total => state.total + amount}}

      {:error, _reason} = r ->
        {:reply, r, state}
    end
  end

  def handle_call(:recent_pledges, _from, state) do
    {:reply, {:ok, state.recent}, state}
  end

  def handle_call(:total_pledged, _from, state) do
    {:reply, {:ok, state.total}, state}
  end

  def handle_cast(:clear, state) do
    {:noreply, %State{:cachesize => state.cachesize}}
  end

  def handle_cast({:set_cache_size, new_size}, state) do
    {:noreply, %State{state |
      :cachesize => new_size,
      :recent => Enum.take(state.recent, new_size)}}
  end

  def handle_info(message, state) do
    IO.puts("unhandled message: #{IO.inspect message}")
    {:noreply, state}
  end

  # Interface with external service.
  defp send_pledge_to_service(name, amount) do
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end

  defp fetch_recent_pledges() do
    # Example result
    #[{"Fred", 20}, {"Barney", 8.5}]
    []
  end
end
