defmodule PledgeServerTest do
  use ExUnit.Case, max_cases: 1
  doctest Servy.PledgeServer

  alias Servy.PledgeServer

  test "no pledges at start" do
    PledgeServer.start_link([])
    assert PledgeServer.recent_pledges == {:ok, []}
    PledgeServer.stop()
  end

  @pledges [{"a", 100}, {"b", 200}, {"c", 300}, {"d", 400}]
  test "max 3 pledges" do
    PledgeServer.start_link([])

    # Populate pledges
    @pledges |> Enum.each(fn {name, amount} ->
      IO.puts(:stderr, "adding: #{name} +#{amount}")
      PledgeServer.create_pledge(name, amount) end)

    # Retrieve cache
    {:ok, pledges} = PledgeServer.recent_pledges()
    assert length(pledges) == 3

    # Total should include ALL the pledges, not just the cache.
    {:ok, total} = PledgeServer.total_pledged()
    sum = @pledges
      |> Enum.reduce(0, fn {_, x}, acc -> acc + x end)
    assert total == sum
    
    PledgeServer.stop()
  end
end


