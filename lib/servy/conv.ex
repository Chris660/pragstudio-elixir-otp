defmodule Servy.Conv do
  defstruct method: "",
            path: "",
            headers: %{},
            params: %{},
            status: nil,
            resp_headers: %{"Content-Type" => "text/html"},
            resp_body: ""

  def format_status(%Servy.Conv{} = conv) do
    "#{conv.status} #{status_reason(conv.status)}"
  end

  defp status_reason(200), do: "OK"
  defp status_reason(201), do: "Created"
  defp status_reason(401), do: "Unauthorized"
  defp status_reason(403), do: "Forbidden"
  defp status_reason(404), do: "Not found"
  defp status_reason(500), do: "Internal server error"
end
