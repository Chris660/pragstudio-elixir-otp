defmodule Servy.SensorServer do
  defmodule State do
    defstruct sensor_data: %{},
      update_interval: :timer.hours(1),
      update_timer: nil
  end

  use GenServer
  alias Servy.VideoCam
  alias Servy.SensorServer.State

  @name __MODULE__

  # Client interfaces
  def start_link do
    start_link(%State{})
  end

  def start_link(interval) do
    IO.puts "starting the sensor server with refresh interval #{interval} ..."
    GenServer.start_link(__MODULE__, %State{update_interval: interval}, name: @name)
  end

  def get_sensor_data() do
    GenServer.call(@name, :get_sensor_data)
  end

  def set_update_interval(interval) when is_integer(interval)
                                     and interval > 0 do
    GenServer.call(@name, {:set_update_interval, interval})
  end

  def stop() do
    GenServer.stop(@name)
  end

  # GenServer callbacks
  def init(%State{} = state) do
    {:ok, update(state)}
  end

  def handle_call(:get_sensor_data, _from, state) do
    {:reply, state.sensor_data, state}
  end

  def handle_call({:set_update_interval, interval}, _from, state) do
    # Start the new update interval immediately.
    Process.cancel_timer(state.update_timer)
    new_state = update(%State{state | update_interval: interval})
    {:reply, :ok, new_state}
  end

  def handle_info(:update, state) do
    {:noreply, update(state)}
  end

  defp schedule_update(interval) do
    Process.send_after(self(), :update, interval)
  end

  defp update(%State{} = state) do
    t_ref = schedule_update(state.update_interval)
    new_data = run_get_sensor_data()
    %State{state | :sensor_data => new_data, :update_timer => t_ref}
  end

  defp run_get_sensor_data() do
    # Send async requests (before Task, used Servy.Fetcher).
    cams = ["cam-1", "cam-2", "cam-3", "cam-4"]
    |> Enum.map(&Task.async(fn -> VideoCam.snapshot(&1) end))

    bf = Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)

    # Await results
    snapshots = cams |> Enum.map(fn task -> Task.await(task) end)
    bigfoot_location = Task.await(bf)
    %{snapshots: snapshots, location: bigfoot_location}
  end
end
