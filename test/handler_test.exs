defmodule HandlerTest do
  use ExUnit.Case
  doctest Servy.Handler
  alias Servy.Handler

  @server_root Path.expand("..", __DIR__)
  @static_root Path.join(@server_root, "pages")

  test "GET /wildthings" do
    request = mkrequest("GET", "/wildthings")
    response = Servy.Handler.handle(request)

    assert response ==
             mkresponse(200, "OK", "text/html", "Bears, Tigers, Lions")
  end

  test "GET /bears" do
    request = mkrequest("GET", "/bears")
    response = Servy.Handler.handle(request)
    expected = mkresponse(200, "OK", "text/html", 395, """
    <h1>All the bears</h1>
    <ul>
      <li>Brutus - Grizzly (6)</li>
      <li>Iceman - Polar (9)</li>
      <li>Kenai - Grizzly (10)</li>
      <li>Paddington - Brown (3)</li>
      <li>Roscoe - Panda (8)</li>
      <li>Rosie - Black (7)</li>
      <li>Scarface - Grizzly (4)</li>
      <li>Smokey - Black (2)</li>
      <li>Snow - Polar (5)</li>
      <li>Teddy - Brown (1)</li>
    </ul>
    """)

    assert remove_whitespace(response) == remove_whitespace(expected)
  end

  test "GET /bigfoot" do
    request = mkrequest("GET", "/bigfoot")
    response = Servy.Handler.handle(request)

    assert response == mkresponse(404, "Not found", "text/html")
  end

  test "GET /wildlife (alias)" do
    request = mkrequest("GET", "/wildlife")
    response = Servy.Handler.handle(request)

    assert response == mkresponse(200, "OK", "text/html", "Bears, Tigers, Lions")
  end

  test "GET /bears?id=2" do
    request = mkrequest("GET", "/bears?id=2")
    response = Servy.Handler.handle(request)
    expected = mkresponse(200, "OK", "text/html", 74,
      "<h1>Show Bear</h1><p>Is Smokey hibernating?<strong>false</strong></p>")

    assert remove_whitespace(response) == remove_whitespace(expected)
  end

  test "GET /about" do
    request = mkrequest("GET", "/about")
    response = Servy.Handler.handle(request)
    body = getfile("about.html")

    assert response == mkresponse(200, "OK", "text/html", body)
  end

  test "GET /pages/spanish/about.html" do
    request = mkrequest("GET", "/pages/spanish/about.html")
    response = Servy.Handler.handle(request)
    body = getfile("spanish/about.html")

    assert response == mkresponse(200, "OK", "text/html", body)
  end

  test "DELETE /bears?id=6" do
    request = mkrequest("DELETE", "/bears?id=6")
    response = Servy.Handler.handle(request)

    assert response == mkresponse(200, "OK", "text/html",
      "Deleted bear 6 (Brutus)!")
  end

  test "POST /bears/new" do
    request = """
    POST /bears/new HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Length: 21\r
    Content-Type: application/x-www-form-urlencoded\r
    \r
    name=Baloo&type=Brown
    """
    response = Servy.Handler.handle(request)

    assert response == mkresponse(201, "Created", "text/html",
      "Created a Brown bear named Baloo!")
  end

  test "GET /api/bears" do
    request = mkrequest("GET", "/api/bears")
    response = Servy.Handler.handle(request)
    expected = mkresponse(200, "OK", "application/json", 605,
      """
      [{"type":"Brown","name":"Teddy","id":1,"hibernating":true},
       {"type":"Black","name":"Smokey","id":2,"hibernating":false},
       {"type":"Brown","name":"Paddington","id":3,"hibernating":false},
       {"type":"Grizzly","name":"Scarface","id":4,"hibernating":true},
       {"type":"Polar","name":"Snow","id":5,"hibernating":false},
       {"type":"Grizzly","name":"Brutus","id":6,"hibernating":false},
       {"type":"Black","name":"Rosie","id":7,"hibernating":true},
       {"type":"Panda","name":"Roscoe","id":8,"hibernating":false},
       {"type":"Polar","name":"Iceman","id":9,"hibernating":true},
       {"type":"Grizzly","name":"Kenai","id":10,"hibernating":false}]
      """)
    assert remove_whitespace(response) == remove_whitespace(expected)
  end

  test "POST /api/bears/new" do
    request = """
    POST /api/bears/new HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/json\r
    Content-Length: 21\r
    \r
    {"name": "Breezly", "type": "Polar"}
    """

    response = Servy.Handler.handle(request)

    assert response == """
    HTTP/1.1 201 Created\r
    Content-Length: 35\r
    Content-Type: text/html\r
    \r
    Created a Polar bear named Breezly!
    """
  end

  test "GET /pages/faq.md" do
    request = mkrequest("GET", "/pages/faq.md")
    response = Servy.Handler.handle(request)
    expected = mkresponse(200, "OK", "text/html",
      """
      <h1>Frequently Asked Questions</h1>
      <ul>
      <li><p><strong>Have you really seen Bigfoot?</strong></p>
      <p>  Yes! In this <a href=\"https://www.youtube.com/watch?v=v77ijOO8oAk\">totally believable video</a>!</p>
      </li>
      <li><p><strong>No, I mean seen Bigfoot <em>on the refuge</em>?</strong></p>
      <p>  Oh! Not yet, but we’re still looking…</p>
      </li>
      <li><p><strong>Can you just show me some code?</strong></p>
      <p>  Sure! Here’s some Elixir:</p>
      </li>
      </ul>
      <pre><code class=\"elixir\">  [&quot;Bigfoot&quot;, &quot;Yeti&quot;, &quot;Sasquatch&quot;] |&gt; Enum.random()</code></pre>
      """)
    assert response == expected
  end

  ##
  ## Helpers
  ##

  defp mkrequest(req, path, body \\ "") do
    """
    #{req} #{path} HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    #{body}
    """
  end

  defp mkresponse(status_code, status_msg, content_type, body \\ "") do
    """
    HTTP/1.1 #{status_code} #{status_msg}\r
    Content-Length: #{byte_size(body)}\r
    Content-Type: #{content_type}\r
    \r
    #{body}
    """
  end

  defp mkresponse(status_code, status_msg, content_type, length, body) do
    """
    HTTP/1.1 #{status_code} #{status_msg}\r
    Content-Length: #{length}\r
    Content-Type: #{content_type}\r
    \r
    #{body}
    """
  end

  defp getfile(filename) do
    {:ok, data} = Path.join(@static_root, filename) |> File.read()
    data
  end

  defp remove_whitespace(text) do
    String.replace(text, ~r{\s}, "")
  end 

end

