defmodule Servy.View do
  @server_root Path.expand("../..", __DIR__)
  @template_root Path.join(@server_root, "templates")

  def render(template, bindings \\ []) do
    Path.join(@template_root, template)
    |> EEx.eval_file(bindings)
  end
end

defmodule Servy.BearView do
  require EEx

  @server_root Path.expand("../..", __DIR__)
  @template_root Path.join(@server_root, "templates")

  EEx.function_from_file :def, :index,
    Path.join(@template_root, "index.eex"), [:bears]

  EEx.function_from_file :def, :show,
    Path.join(@template_root, "show.eex"), [:bear]
end
