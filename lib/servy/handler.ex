require Regex

defmodule Servy.Handler do
  @moduledoc "Handles HTTP requests, serves responses."

  import Servy.Response.Utils, only: [
    log: 1, rewrite_path: 1, track: 1, calc_content_length: 1]
  import Servy.Parser, only: [parse: 1]
  import Servy.FileHandler, only: [serve_static: 2]

  alias Servy.Conv
  alias Servy.BearController
  alias Servy.SensorServer
  alias Servy.VideoCam
  alias Servy.View

  @doc "Handles `request`, returns a response."
  def handle(request) do
    request
    |> parse
    # |> log
    |> rewrite_path
    |> route
    # |> log
    |> track
    |> calc_content_length
    |> format_response
  end

  defp route(%Conv{method: "GET", path: "/sensors"} = conv) do
    data = SensorServer.get_sensor_data()
    body = View.render("bigfoot_location.eex",
      snapshots: data.snapshots, location: data.location)
    %{conv | :status => 200, :resp_body => body}
  end

  defp route(%Conv{method: "GET", path: "/hibernate/" <> time} = conv) do
    time |> String.to_integer |> :timer.sleep
    %{conv | :status => 200, :resp_body => "Awake!"}
  end

  defp route(%Conv{method: "GET", path: "/wildthings"} = conv) do
    %{conv | :status => 200, :resp_body => "Bears, Tigers, Lions"}
  end

  defp route(%Conv{method: "GET", path: "/api/bears"} = conv) do
    Servy.Api.BearController.index(conv)
  end

  defp route(%Conv{method: "POST", path: "/api/bears/new"} = conv) do
    Servy.Api.BearController.create(conv, conv.params)
  end

  defp route(%Conv{method: "GET", path: "/pledges/new"} = conv) do
    Servy.PledgeController.new(conv)
  end

  defp route(%Conv{method: "GET", path: "/pledges"} = conv) do
    Servy.PledgeController.index(conv)
  end

  defp route(%Conv{method: "POST", path: "/pledges"} = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end

  defp route(%Conv{method: "GET", path: "/bears"} = conv) do
    BearController.index(conv)
  end

  defp route(%Conv{method: "GET", path: "/bears/" <> id} = conv) do
    BearController.show(conv, Map.put(conv.params, "id", id))
  end

  defp route(%Conv{method: "POST", path: "/bears/new"} = conv) do
    BearController.create(conv, conv.params)
  end

  defp route(%Conv{method: "GET", path: "/bears/new"} = conv) do
    serve_static("new_bear.html", conv)
  end

  defp route(%Conv{method: "GET", path: "/about"} = conv) do
    serve_static("about.html", conv)
  end

  defp route(%Conv{method: "GET", path: "/pages/" <> page_name} = conv) do
    serve_static(page_name, conv)
  end

  defp route(%Conv{method: "DELETE", path: "/bears/" <> id} = conv) do
    BearController.delete(conv, Map.put(conv.params, "id", id))
  end

  defp route(%Conv{method: "GET", path: "/404s"} = conv) do
    stats = Servy.FourOhFourCounter.get_counts()
    %{conv |
      :status => 200,
      :resp_body => View.render("404s.eex", stats: Map.to_list(stats))}
  end

  defp route(%Conv{} = conv) do
    %{conv | :status => 404}
  end

  defp format_response_headers(header_map) do
    header_map
    |> Enum.map(fn {key, value} -> "#{key}: #{value}" end)
    |> Enum.sort
    |> Enum.join("\r\n")
  end

  defp format_response(%Conv{} = conv) do
    header_lines = format_response_headers(conv.resp_headers)

    """
    HTTP/1.1 #{Conv.format_status(conv)}\r
    #{header_lines}\r
    \r
    #{conv.resp_body}
    """
  end
end
