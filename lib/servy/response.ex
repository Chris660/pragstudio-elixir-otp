require Logger

defmodule Servy.Response.Utils do
  alias Servy.Conv
  alias Servy.FourOhFourCounter

  def log(conv) do
    if Mix.env != :test do
      IO.inspect(%Conv{} = conv)
    else
      conv
    end
  end

  def rewrite_path(%Conv{path: path} = conv) do
    pattern = ~r{\/(?<thing>\w+)\?id=(?<id>\d+)}
    captures = Regex.named_captures(pattern, path)
    rewrite_path_captures(conv, captures)
  end

  defp rewrite_path_captures(conv, %{"thing" => thing, "id" => id}) do
    %{conv | path: "/#{thing}/#{id}"}
  end

  defp rewrite_path_captures(%{path: "/wildlife"} = conv, nil) do
    %{conv | path: "/wildthings"}
  end

  defp rewrite_path_captures(conv, nil), do: conv

  @doc "Log requests along with their response status."
  def track(%Conv{method: method, status: status, path: path} = conv) do

    if status == 404 do
      FourOhFourCounter.bump_count(path)
    end

    if Mix.env == :dev do
      status_log_level(status).("#{method} #{path} #{status}")
    end
    conv
  end

  def track(%Conv{} = conv), do: conv

  def put_resp_header(%Conv{} = conv, key, value) do
    headers = Map.put(conv.resp_headers, key, value)
    %{conv | resp_headers: headers}
  end
    
  def calc_content_length(%Conv{} = conv) do
    # NB. Content-Length must contain the response length in *bytes*
    # not characters.
    put_resp_header(conv, "Content-Length", byte_size(conv.resp_body))
  end

  defp status_log_level(200), do: &Logger.info(&1)
  defp status_log_level(201), do: &Logger.info(&1)
  defp status_log_level(401), do: &Logger.warn(&1)
  defp status_log_level(403), do: &Logger.warn(&1)
  defp status_log_level(404), do: &Logger.warn(&1)
  defp status_log_level(_), do: &Logger.error(&1)
end
