defmodule Servy.Wildthings do
  require Logger
  alias Servy.Bear

  @server_root Path.expand("../..", __DIR__)
  @db_file Path.join(@server_root, "db/bears.json")

  def list_bears do
    read_json(@db_file)
    |> Poison.decode!(as: %{"bears" => [%Bear{}]})
    |> Map.get("bears")
  end

  defp read_json(file) do
    case File.read(file) do
      {:ok, content} ->
        content
      {:error, reason} ->
        Logger.error("reading #{file} failed: #{reason}")
        "[]"
    end
  end

  def get_bear(id) when is_integer(id) do
    Enum.find(list_bears(), fn(b) -> b.id == id end)
  end

  def get_bear(id) when is_binary(id) do
    id |> String.to_integer |> get_bear
  end
end
