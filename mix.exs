defmodule Servy.MixProject do
  use Mix.Project

  def project do
    [
      app: :servy,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      # Other (standard) applications to start
      extra_applications: [:logger],

      # Define the module implementing the Application behaviour that
      # starts our application.
      # The tuple has the form {Module, args}, and OTP will call: Module.start(args)
      mod: {Servy, []},

      # Define the environment for our application.
      # These environment variables may be overridden on the command line:
      #
      #   elixir --erl "-servy port 5432" -S mix run --no-halt
      #
      # You can also override them locally for this project, be editing 
      # config/config.exs, e.g.:
      #
      #   config :servy, port: 3210
      #
      env: [port: 3000]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:earmark, "~> 1.3"},
      {:httpoison, "~> 1.5"},
      {:poison, "~> 4.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
