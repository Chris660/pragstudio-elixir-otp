defmodule Servy.FourOhFourCounter do
  @name __MODULE__

  # By "using" GenServer we inject default callbacks for the behaviour.
  use GenServer

  # Client interfaces
  def start_link(_) do
    GenServer.start_link(__MODULE__, Map.new(), name: @name)
  end

  def stop() do
    GenServer.cast @name, :stop
  end

  def bump_count(path) do
    GenServer.cast @name, {:bump_count, path}
  end

  def get_count(path) do
    GenServer.call @name, {:get_count, path}
  end

  def get_counts do
    GenServer.call @name, :get_counts
  end

  # GenServer callbacks
  def init(args) do
    {:ok, args}
  end

  def handle_cast({:bump_count, path}, state) do
    {:noreply, state |> Map.update(path, 1, &(&1 + 1))}
  end

  def handle_cast(:stop, state) do
    {:stop, :stopped, state}
  end

  def handle_call({:get_count, path}, _from, state) do
    {:reply, Map.get(state, path, 0), state}
  end

  def handle_call(:get_counts, _from, state) do
    {:reply, state, state}
  end
end
