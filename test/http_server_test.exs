defmodule HttpServerTest do
  use ExUnit.Case
  doctest Servy.HttpServer

  alias Servy.HttpServer

  @host 'localhost'
  @port 4321

  test "GET /wildthings" do
    # Start server, make request, then kill it.
    server = spawn(HttpServer, :start, [@port])
    {:ok, response} = HTTPoison.get("http://#{@host}:#{@port}/wildthings")
    Process.exit(server, :kill)
    assert response.status_code == 200
    assert response.body == "Bears, Tigers, Lions"
  end

  test "GET /wildthings (concurrent)" do
    # Start server, make request, then kill it.
    server = spawn(HttpServer, :start, [@port])

    base_url = "http://#{@host}:#{@port}"

    ["/wildthings", "/bears", "/bears/3"]
    |> Enum.map(&Task.async(fn -> HTTPoison.get!(base_url <> &1) end))
    |> Enum.map(&Task.await/1)
    |> Enum.each(fn response ->
         assert response.status_code == 200;
       end)

    Process.exit(server, :kill)
  end
end

