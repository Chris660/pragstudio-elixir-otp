defmodule Servy.GenericServer do
  def start(name, module, state) do
    pid = spawn(__MODULE__, :listen_loop, [{module, state}])
    Process.register(pid, name)
    name
  end

  def call(name, request, timeout \\ 1000) do
    send name, {self(), request}
    receive do
      {:response, response} ->
        response

      after timeout ->
        {:error, :timeout}
    end
  end

  def cast(name, request) do
    send name, {:cast, request}
  end

  #
  # Server process functions
  #
  def listen_loop({module, state}) do
    receive do
      {:cast, :stop} ->
        :ok

      {:cast, message} ->
        new_state = module.handle_cast(message, state)
        listen_loop({module, new_state})

      {sender, message} when is_pid(sender) ->
        {response, new_state} = module.handle_call(message, state)
        send sender, {:response, response}
        listen_loop({module, new_state})

      unhandled ->
        IO.puts "unhandled message: #{inspect unhandled}"
        listen_loop({module, state})
    end
  end
end
