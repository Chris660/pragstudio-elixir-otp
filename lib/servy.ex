defmodule Servy do
  use Application

  def start(type, args) do
    IO.puts "Starting Servy application (type: #{type}), args: #{args}."
    Servy.Supervisor.start_link(args)
  end
end
