defmodule Servy.ServicesSupervisor do
  use Supervisor

  def start_link(_) do
    IO.puts "starting the services supervisor."
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    # NB. the Supervisor will call MODULE.child_spec() on GenServers to
    # determine how to start these children.
    #
    #   iex(8)> Servy.PledgeServer.child_spec([])
    #   %{id: Servy.PledgeServer, start: {Servy.PledgeServer, :start_link, [[]]}}
    #
    # You can customise the start-up behaviour by overriding child_spec/1 in the
    # GenServer, or by passing parameters to the `use` declaration, e.g.:
    #
    #   use GenServer, restart: :permanent
    #
    children = [
      Servy.PledgeServer,
      {Servy.SensorServer, :timer.minutes(2)},
      Servy.FourOhFourCounter,
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
