defmodule Servy.Fetcher do
  def async_request(func) do
    caller = self()
    spawn(fn ->
      send(caller, {:fetcher, self(), func.()})
    end)
  end

  def await_result(pid, timeout \\ 2000) do
    receive do
      {:fetcher, ^pid, result} ->
        result
      after timeout ->
        raise "Timed out!"
    end
  end
end
