defmodule Servy.BearController do
  alias Servy.Bear
  alias Servy.Conv
  alias Servy.Wildthings
  alias Servy.BearView

  def index(%Conv{} = conv) do
    bears =
      Wildthings.list_bears()
      |> Enum.sort(&Bear.name_lower?/2)

    %{conv | :status => 200, :resp_body => BearView.index(bears)}
  end

  def show(%Conv{} = conv, %{"id" => id}) do
    bear = Wildthings.get_bear(id)
    %{conv | :status => 200, :resp_body => BearView.show(bear)}
  end

  def create(%Conv{} = conv, %{"name" => name, "type" => type}) do
    %{conv
      | status: 201,
        resp_body: "Created a #{type} bear named #{name}!"}
  end

  def delete(%Conv{} = conv, %{"id" => id}) do
    case Wildthings.get_bear(id) do
      %Bear{name: name} ->
        %{conv | status: 200, resp_body: "Deleted bear #{id} (#{name})!"}
      nil ->
        %{conv | status: 404, resp_body: "Bear #{id} not found"}
    end
  end
end
